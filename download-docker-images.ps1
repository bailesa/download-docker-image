﻿[CmdletBinding()]
param (
    [Parameter(Mandatory = $true, ParameterSetName = 'Image', HelpMessage = "Image name from registry")]
    [string]$image,

    [Parameter(Mandatory = $false, ParameterSetName = 'Tag', HelpMessage = "Tag of image. Default is latest")]
    [string]$tag
)

if ([string]::IsNullOrWhiteSpace($tag)) {
    $tag = "latest"
}

$imageuri = "https://auth.docker.io/token?service=registry.docker.io&scope=repository:${image}:pull"
$taguri = "https://registry-1.docker.io/v2/${image}/manifests/${tag}"
$bloburi = "https://registry-1.docker.io/v2/${image}/blobs/"

# Generate folder to save image files within
$path = "$image$tag" -replace '[\\/":*?<>|]'
if (!(test-path $path)) {
    New-Item -ItemType Directory -Force -Path $path
}

# Get a token from registry
$token = Invoke-WebRequest -Uri $imageuri | ConvertFrom-Json | Select-Object -expand token

# Get image manifest
$headers = @{}
$headers.add("Authorization", "Bearer $token")
# this header is needed to get manifest in correct format: https://docs.docker.com/registry/spec/manifest-v2-2/
$headers.add("Accept", "application/vnd.docker.distribution.manifest.v2+json")
$manifest = Invoke-Webrequest -Headers $headers -Method GET -Uri $taguri | ConvertFrom-Json

# Download config json
$configSha = $manifest | Select-Object -expand config | Select-Object -expand digest
$config = ".\$path\config.json"
Invoke-Webrequest -Headers @{Authorization = "Bearer $token" } -Method GET -Uri $bloburi$configSha -OutFile $config

# Generate manifest.json
$manifestJson = @{}
$manifestJson.add("Config", "config.json")
$manifestJson.add("RepoTags", @("${image}:${tag}"))

# Download image layers
$layers = $manifest | Select-Object -expand layers | Select-Object -expand digest
$blobtmp = ".\$path\blobtmp"

#Download image blobs
$layersJson = @()
foreach ($blobelement in $layers) {
    # Update filename so name doesnt start with 'sha256:'
    $fileName = "$blobelement.gz" -replace 'sha256:'
    $newfile = ".\$path\$fileName"
    $layersJson += @($fileName)

    # Token expires after 5 minutes, so request a new one for every blob just in case
    $token = Invoke-WebRequest -Uri $imageuri | ConvertFrom-Json | Select-Object -expand token
    Invoke-Webrequest -Headers @{Authorization = "Bearer $token" } -Method GET -Uri $bloburi$blobelement -OutFile $blobtmp
    Copy-Item $blobtmp $newfile -Force -Recurse
}

# Remove temporary blob
Remove-Item $blobtmp 

# Save manifest.json
$manifestJson.add("Layers", $layersJson)
ConvertTo-Json -Depth 5 -InputObject @($manifestJson) | Out-File -Encoding ascii ".\$path\manifest.json"

# Create tar.gz of image
Set-Location .\$path
tar cvf "$path.tar.gz" *

Write-Output "Transfer generated $path.tar.gz to network and then run below command"
Write-Output "docker load < $path.tar.gz"